﻿using System;

/// <summary>
/// Summary description for Class1
/// </summary>
public class StudentModel
{
	public string Name { get; set; }
    public string Gender { get; set; }
    public string Address { get; set; }
    public string EmailId { get; set; }
    public string PhoneNo { get; set; }
    public string BloodGroup { get; set; }
    public string OfficeLocation { get; set; }


}
